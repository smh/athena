
#include "FTK_RecTools/FTK_VertexFinderTool.h"
#include "FTK_RecTools/FTK_PixelClusterOnTrackTool.h"
#include "FTK_RecTools/FTK_SCTClusterOnTrackTool.h"
#include "FTK_RecTools/FTK_DuplicateTrackRemovalTool.h"
#include "FTK_RecTools/FTK_HashIDTool.h"

DECLARE_COMPONENT( FTK_VertexFinderTool )
DECLARE_COMPONENT( FTK_HashIDTool )
DECLARE_COMPONENT( FTK_PixelClusterOnTrackTool )
DECLARE_COMPONENT( FTK_SCTClusterOnTrackTool )
DECLARE_COMPONENT( FTK_DuplicateTrackRemovalTool )

